<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div style="margin: 10px;">
	<h4>List of Users</h4>
	<table style="width: 100%" class="reference" border="1">
		<tbody>
			<tr>
				<th>No. (count)</th>
				<th>ID</th>
				<th>NAME</th>
				<th>EMAIL</th>
				<th>SEX</th>
				<th>COUNTRY</th>
				<th>EDIT</th>
				<th>DELETE</th>
			</tr>
			<c:forEach var="usuario" items="${requestScope.persons}" varStatus="loopCounter">
				<tr>
					<td><c:out value="${loopCounter.count}" /></td>
					<td align="center">${usuario.id}</td>
					<td id="name">${usuario.name}</td> 
					<td id="email">${usuario.email}</td>
					<td id="sex" align="center">${usuario.sex}</td>
					<td id="country">${usuario.country}</td>
					<td id="edit" align="center"><a href="edit/${usuario.id}">Edit User</a></td>
					<td id="edit" align="center"><a href="delete/${usuario.id}">Delete User</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>