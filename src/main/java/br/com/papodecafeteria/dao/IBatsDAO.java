package br.com.papodecafeteria.dao;

import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import com.mysql.cj.jdbc.MysqlDataSource;

import br.com.papodecafeteria.dao.mapper.User4JMapper;
import br.com.papodecafeteria.model.User4J;

public class IBatsDAO {
	
	private static Logger l = Logger.getLogger(IBatsDAO.class.getName());
	
	private static DataSource getMySQLDataSource() {
		Properties props = new Properties();
		MysqlDataSource pMysqlDataSource = null;
		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			props.load(loader.getResourceAsStream("db.property"));
			pMysqlDataSource = new MysqlDataSource();
			pMysqlDataSource.setURL(props.getProperty("MYSQL_DB_URL"));
			pMysqlDataSource.setUser(props.getProperty("MYSQL_DB_USERNAME"));
			pMysqlDataSource.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pMysqlDataSource;
	}
	
	private static SqlSession getSqlSession(){
		SqlSession pSqlSession = null;
		try {
			TransactionFactory transactionFactory = new JdbcTransactionFactory();

			Environment environment = new Environment("development", transactionFactory, getMySQLDataSource());

			Configuration configuration = new Configuration(environment);
			configuration.getTypeAliasRegistry().registerAlias(User4JMapper.class);
			configuration.addMapper(User4JMapper.class);

			SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
			SqlSessionFactory factory = builder.build(configuration);
			pSqlSession = factory.openSession(true); // Autocommit = true
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pSqlSession;
	}
	
	public static User4J getById(int pId){
		User4J pUser4J = null;
		try {
			pUser4J = getSqlSession().getMapper(User4JMapper.class).selectUser4JById(pId);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pUser4J;
	}
	
	public static List<User4J> getAll(){
		List<User4J> lstUserJPA = null;
		try {
			SqlSession pSqlSession = getSqlSession();
			lstUserJPA = pSqlSession.getMapper(User4JMapper.class).selectListUser4J();
			pSqlSession.close();
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return lstUserJPA;
	}
	
	public static void delete(int pId){
		try{
			SqlSession pSqlSession = getSqlSession();
			pSqlSession.getMapper(User4JMapper.class).deleteUser4JById(pId);
			pSqlSession.close();
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void update(User4J pUser4J){
		try{
			SqlSession pSqlSession = getSqlSession();
			pSqlSession.getMapper(User4JMapper.class).updateUser4J(pUser4J);
			pSqlSession.close();
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void insert(User4J pUser4J){
		try{
			SqlSession pSqlSession = getSqlSession();
			pSqlSession.getMapper(User4JMapper.class).insertUser4J(pUser4J);
			pSqlSession.close();
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
}
