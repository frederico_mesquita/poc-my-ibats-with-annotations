package br.com.papodecafeteria.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

import br.com.papodecafeteria.model.User4J;

public interface User4JMapper {
	@Select("select id, name, email, sex, country from user where id = #{pId}")
	User4J selectUser4JById(int pId);

	@Select("select id, name, email, sex, country from user")
	@Results(value = {
		@Result(property = "id",	 column = "id", 		jdbcType = JdbcType.INTEGER, id = true),
        @Result(property = "name",	 column = "name", 		jdbcType = JdbcType.VARCHAR),
        @Result(property = "email",  column = "email", 		jdbcType = JdbcType.VARCHAR),
        @Result(property = "sex", 	 column = "sex", 		jdbcType = JdbcType.VARCHAR),
        @Result(property = "country", column = "country", 	jdbcType =  JdbcType.VARCHAR)
	})
	List<User4J> selectListUser4J();
	
	@Delete("delete from user where id = #{pId}")
	void deleteUser4JById(int pId);
	
	String cUpdate = "UPDATE USER SET name = #{name}, email = #{email}, sex = #{sex}, country = #{country} WHERE  id = #{id}";
	@Update(cUpdate)
	void updateUser4J(User4J pUser4J);
	
	String cInsert = "insert into USER(name, password, email, sex, country) values (#{name}, #{password}, #{email}, #{sex}, #{country})"; 
	@Insert(cInsert)
	int insertUser4J(User4J pUser4J);
}
